"""
Pull requests

Account

User
"""

import json

import requests

from debugging import pretty_print as pp, debug_response
from constants import (BITBUCKET_API_KEY, BITBUCKET_API_SECRET,
                       BITBUCKET_OAUTH_ENDPOINT, BITBUCKET_API_ENDPOINT,
                       BITBUCKET_API_REFRESH_TOKEN)
from authorization import authorize, authenticate, get_refresh_token


def get_user(access_token):
    response = requests.get(
        f'{BITBUCKET_API_ENDPOINT}/2.0/user',
        headers={
            'Authorization': f'Bearer {access_token}'
        }
    )
    return response.json()


def get_workspaces(access_token):
    response = requests.get(
        f'{BITBUCKET_API_ENDPOINT}/2.0/workspaces',
        headers={
            'Authorization': f'Bearer {access_token}'
        }
    )
    return response.json()


def get_workspaces_repositories(access_token):
    workspaces = get_workspaces(access_token)
    for workspace in workspaces['values']:
        repositories = requests.get(
            workspace['links']['repositories']['href'],
            headers={
                'Authorization': f'Bearer {access_token}'
            }
        ).json()
        for repository in repositories['values']:
            print('repository')
            pp(repository)
            pull_requests = requests.get(
                repository['links']['pullrequests']['href'],
                headers={
                    'Authorization': f'Bearer {access_token}'
                }
            ).json()
            print('pull_requests')
            pp(pull_requests)


def get_workspaces_users(access_token):
    workspaces = get_workspaces(access_token)
    member_hrefs = []
    for workspace in workspaces['values']:
        members = requests.get(
            workspace['links']['members']['href'],
            headers={
                'Authorization': f'Bearer {access_token}'
            }
        ).json()
        for member in members['values']:
            member_hrefs.append(member['links']['self']['href'])
    for member_href in member_hrefs:
        member = requests.get(
            member_href,
            headers={
                'Authorization': f'Bearer {access_token}'
            }
        ).json()
        yield requests.get(
            member['user']['links']['self']['href'],
            headers={
                'Authorization': f'Bearer {access_token}'
            }
        ).json()


def main():
    access_token = get_refresh_token(
        BITBUCKET_API_KEY,
        BITBUCKET_API_SECRET,
        BITBUCKET_API_REFRESH_TOKEN
    )['access_token']
    workspaces_repositories = get_workspaces_repositories(access_token)
