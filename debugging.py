import json


def pretty_print(item):
    print(json.dumps(item, indent=4))


def debug_response(response):
    print(f'{response.url} {response.status_code}')
    for header in response.headers:
        print(f'{header}: {response.headers[header]}')
    if response.text:
        pretty_print(response.json())
