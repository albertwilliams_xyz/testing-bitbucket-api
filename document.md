![BitBucket](https://seekvectorlogo.com/wp-content/uploads/2019/04/bitbucket-vector-logo.png)

# Introduction

This document contains basic information for connecting to and extracting data from BitBucket services as a third party service. And how to integrate it in our application.

## Basic information

| Information | Description | 
| --- | --- |
| Product Name | BitBucket |
| Website | https://bitbucket.org/ |
| Developer documentation | https://developer.atlassian.com/bitbucket/ |
| Description | BitBucket provides a mean to manage repositories for personal and organization projects |

## Authentication and Authorization

BitBucket supports OAuth as its mean of authentication for its REST API. The process to get authenticated and start making use of the API si pretty straightforward:

1. Get a code from the `https://bitbucket.org/site/oauth2/authorize` endpoint, 

On the following [link](https://developer.atlassian.com/bitbucket/api/2/reference/meta/authentication) you will be able to find more information about authentication in BitBucket













Step for each integration

1- Spike, research to understand how we can integrate with the providers(at least you should be able to invoke the API to validate we have access)

2- Demo populate laika objects tables using a test account

3- Define additional tickets based on Product feedback

4- Make integration production ready 


Important questions

What kind of laika objects do we want to pull from this integration?(Product should help us with this information) 
Once you have the laika objects, please review the  official list of LOs here under the tabs starting with “object_*”: link for details about each object

 
Can we set up a developer account to test the API? Or do we need to make a manual request to get one?(Please to create accounts use dev@heylaika.com instead of your account)

Does it support OAuth?

If the integration does not support OAuth, explain what type of data we need to request from customers?

Can we spot any security concerns about how the authentication works for this integration(for example store user’s password)?

How we get authorization to access different sets of data. Do we need to specify scopes?

Is there a rate limit in place for API invocations?

Do we need to follow a certification process in order to use the integration in production?

Define which APIs we need to call in order to populate the expected laika objects?

Which columns or expected laika objects can not be pulled because they are not exposed by the provider? 
 

Important Links

The following link contains the attributes or columns expected by Product for each laika object.   
Laika Object Schema
This is a reference, when we start the integration spike we can realize some fields don't make sense or new fields are needed.

ACCOUNT laika object
There is a mapping with a set of minimum fields to fill, but additionally if we can get general settings using the api, the engineer should discuss with product if this settings are useful to be stored in the Account laika object  


