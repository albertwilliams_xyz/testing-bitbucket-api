import requests

from debugging import pretty_print, debug_response
from constants import (BITBUCKET_API_KEY, BITBUCKET_API_SECRET,
                       BITBUCKET_OAUTH_ENDPOINT, BITBUCKET_API_ENDPOINT,
                       BITBUCKET_API_REFRESH_TOKEN)


def authorize(api_key, api_secret):
    payload = { 'client_id': api_key, 'response_type': 'code' }
    response = requests.get(f'{BITBUCKET_OAUTH_ENDPOINT}/authorize', params=payload)
    return response.json()


def authenticate(api_key, api_secret, code):
    payload = { 'grant_type': 'authorization_code', 'code': code }
    response = requests.post(f'{BITBUCKET_OAUTH_ENDPOINT}/access_token', data=payload, auth=(api_key, api_secret))
    return response.json()


def get_refresh_token(api_key, api_secret, refresh_token):
    payload = { 'grant_type': 'refresh_token', 'refresh_token': refresh_token }
    response = requests.post(f'{BITBUCKET_OAUTH_ENDPOINT}/access_token', data=payload, auth=(api_key, api_secret))
    return response.json()
